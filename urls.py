from django.contrib import admin
from django.urls import include, path
urlpatterns = [
path( 'maNouvelleApp/' ,
include( 'maNouvelleApp.urls' )),
path('admin/' , admin.site.urls) ,
]